var AssistanceTrucksPlace = function(name, map) {
    this.name = name;
    this.map = map;
    this.trucksData = [];

    this.addTruck = function(truck) {
        //Creamos el layer en el mapa para ese truck
        var truckLayer = L.layerGroup().addTo(this.map);
        // Agregamos el layer al control
        this.map.layersControl.addOverlay(truckLayer, truck.name);

        var updater = function(newPosition) {
            console.log("Updating view for truck: " + truck.name + "!!");
            console.log(newPosition);
            
            // Opción 1.
            truckLayer.addLayer(L.marker(newPosition).bindPopup(truck.name));
        }

        this.trucksData.push({
            truck: truck,
            updater: updater
        })
    }

    this.start = function() {
        this.trucksData.forEach(function(data) {
            var truck = data.truck;
            truck.run(data.updater);
        });
    }
};