var Trucks = function obtenerDatos(){
    let url = `https://assistanceservices.herokuapp.com/api/supporttrucks`;

    const api = new XMLHttpRequest();

    api.open('GET', url, true);
    api.send();

    api.onreadystatechange = function() {
        if (this.status == 200 && this.readyState == 4) {

            let moviles = JSON.parse(this.responseText);

            for (let item of moviles.supportTrucks){
                imprimirPosicionesPorMovil(item.id);
            }
        }
    }

    function imprimirPosicionesPorMovil(idMovil){
        let url = `https://assistanceservices.herokuapp.com/api/supporttrucks/${idMovil}/positions`;

        const api = new XMLHttpRequest();

        api.open('GET', url, true);
        api.send();

        api.onreadystatechange = function() {
            if (this.status == 200 && this.readyState == 4) {

                let moviles = JSON.parse(this.responseText);

                for (let item of moviles.positions){
                    obtenerEstado(idMovil, item.position.lat, item.position.lon, item.state)
                }
            }
        }
    }

    function obtenerEstado(idMovil, latitud, longitud, idEstado){
        let url = `https://assistanceservices.herokuapp.com/api/truckstates/${idEstado}`

        const api = new XMLHttpRequest();
        api.open('GET', url, true);
        api.send();

        api.onreadystatechange = function() {
            if (this.status == 200 && this.readyState == 4) {
                let estado = JSON.parse(this.responseText)

                console.log("id del movil: ", idMovil)
                console.log("latitud: ", latitud)
                console.log("longitud: ", longitud)
                console.log("estado: ", estado.state.description)
                console.log("--------------")
            }
        }
    }

            // let datos = JSON.parse(this.responseText);
            // console.log(datos.serie);

            // let resultado = document.querySelector('#resultado');
            // resultado.innerHTML = '';

            // let i = 0;

            // for (let item of datos.serie){
            //     i++;
            //     resultado.innerHTML += `<li>${item.fecha.substr(0,10)} | ${item.valor}</li>`
            
            //     if (i>10){
            //         break;
            //     }
            // }
};