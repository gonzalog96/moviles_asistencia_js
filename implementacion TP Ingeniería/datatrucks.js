var DataTrucks = function() {

	this.getListadoMoviles = function(){
		var movilesId = new Array();

		// pegamos a la API para obtener los IDS de cada movil.
	    let url = `https://assistanceservices.herokuapp.com/api/supporttrucks`;

	    const api = new XMLHttpRequest();

	    api.open('GET', url, true);
	    api.send();

	    api.onreadystatechange = function() {
	        if (this.status == 200 && this.readyState == 4) {

	            let moviles = JSON.parse(this.responseText);
	            console.log(moviles);
	            for (let item of moviles.supportTrucks){
	                 movilesId.push(item.id);
	             }
	         }
	     }
	     return movilesId;
	}

	this.obtenerCoordenadasLatPorMovil = function(idMovil){
			var coordenadasLat = [];

         let url = `https://assistanceservices.herokuapp.com/api/supporttrucks/${idMovil}/positions`;

         const api = new XMLHttpRequest();

         api.open('GET', url, true);
         api.send();

         api.onreadystatechange = function() {
             if (this.status == 200 && this.readyState == 4) {

                 let moviles = JSON.parse(this.responseText);

                 for (let item of moviles.positions){
                 	coordenadasLat.push(item.position.lat);
                 }
             }
         }
	}

	this.obtenerCoordenadasLonPorMovil = function(idMovil){
		var coordenadasLon = [];

        let url = `https://assistanceservices.herokuapp.com/api/supporttrucks/${idMovil}/positions`;

        const api = new XMLHttpRequest();

        api.open('GET', url, true);
        api.send();

        api.onreadystatechange = function() {
             if (this.status == 200 && this.readyState == 4) {

                let moviles = JSON.parse(this.responseText);

                for (let item of moviles.positions){
                 	coordenadasLon.push(item.position.lon);
                }
             }
         }
         return coordenadasLon;
	}

	this.obtenerEstadosPorMovil = function(idMovil){
		var estadosPorCoordenadaMovil = [];

		// obtenemos el ID de los estados de cada momento del movil (en cada coordenada en particular);
        let url = `https://assistanceservices.herokuapp.com/api/supporttrucks/${idMovil}/positions`;

        const api = new XMLHttpRequest();

        api.open('GET', url, true);
        api.send();

        api.onreadystatechange = function() {
             if (this.status == 200 && this.readyState == 4) {

                let moviles = JSON.parse(this.responseText);

                for (let item of moviles.positions){
							(function () {
								//obtenemos el String correspondiente al estado del ID.
								let url = `https://assistanceservices.herokuapp.com/api/truckstates/${item.state}`

								const api = new XMLHttpRequest();
								api.open('GET', url, true);
								api.send();

									api.onreadystatechange = function() {
										if (this.status == 200 && this.readyState == 4) {
							    			let estado = JSON.parse(this.responseText)
							    			estadosPorCoordenadaMovil.push(estado.state.description);
							 			}
					 				}
	  						}) ();
                }
             }
          }
         return estadosPorCoordenadaMovil;
       }
 };