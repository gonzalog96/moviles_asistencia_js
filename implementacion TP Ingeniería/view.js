
function bootstrap() {

    // Ubicación de la UNGS.
    var ungsLocation = [-34.5221554, -58.7000067];

    // Creación del componente mapa de Leaflet.
    var map = L.map('mapid').setView(ungsLocation, 15);

    // Agregamos los Layers de OpenStreetMap.
    var baseLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    // Agregamos el control para seleccionar Layers al mapa
    var layersControl = L.control.layers({
        "Base": baseLayer
    });
    layersControl.addTo(map);
    // hack:
    map.layersControl = layersControl;

    // Creamos un círculo con centro en la UNGS.
    var circle = L.circle(ungsLocation, {
        color: '#0000AA',
        fillColor: '#0000CC',
        fillOpacity: 0.2,
        radius: 300
    }).addTo(map);

    // Creamos un marker sobre la UNGS.
    var ungsMarker = L.marker(ungsLocation);
    ungsMarker.addTo(map);

    // Creamos un nuevo recorrido de móviles.
    var truck1K = new AssistanceTrucksPlace("Recorrido de asistencia", map);

    // 25-06-2019
    var dataTrucks = new DataTrucks();

    var prueba = dataTrucks.getListadoMoviles();
    console.log(prueba.id);


    // // truck 501
    // var primerTruck = new Truck("Primer Móvil", [
    //  {lat: -34.524309, lon: -58.695315},
    //  {lat: -34.524048, lon: -58.695630},
    //  {lat: -34.523720, lon: -58.696011},
    //  {lat: -34.523464, lon: -58.696311},
    //  {lat: -34.523076, lon: -58.696773},
    //  {lat: -34.522426, lon: -58.697540},
    //  {lat: -34.522420, lon: -58.697544},
    //  {lat: -34.522102, lon: -58.697934},
    //  {lat: -34.521860, lon: -58.698216},
    //  {lat: -34.521464, lon: -58.698688}
    //     ]);
    // //
    // truck1K.addTruck(primerTruck);

    // // iniciamos el recorrido
    // truck1K.start();
    	
}
$(bootstrap);
    // FIN.
	
	// Pietro!	
 //    var pietro = new Runner("Pietro Maximoff", 'blue', [
	// 	{lat: -34.524309, lon: -58.695315},
	// 	{lat: -34.524048, lon: -58.695630},
	// 	{lat: -34.523720, lon: -58.696011},
	// 	{lat: -34.523464, lon: -58.696311},
	// 	{lat: -34.523076, lon: -58.696773},
	// 	{lat: -34.522426, lon: -58.697540},
	// 	{lat: -34.522420, lon: -58.697544},
	// 	{lat: -34.522102, lon: -58.697934},
	// 	{lat: -34.521860, lon: -58.698216},
	// 	{lat: -34.521464, lon: -58.698688}
 //        ]);
	// //
 //    race1K.addRunner(pietro);

 //    // START!
 //    race1K.start();