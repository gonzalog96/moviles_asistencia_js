
var bootstrap = function() {
    var url = Config.url;
    var urlTrucks = '/supporttrucks/';
    var urlStates = '/truckstates/';
    
    var map = createMap('mapid');
    var drawer = new Drawer();

    // OPCIÓN 3: Promises. Request asincrónico evitando el callbackhell.

    // var requestSupportTruck = function(supportTruck_id) {
    //     return $.ajax(url + urlTrucks + supportTruck_id);
    // }
    // var requestState = function(state_id) {
    //     return $.ajax(url + urlStates + state_id);
    // }
    // var responseExtract = function(attr, response) {
    //     console.log(response);
    //     return response[attr]
    // }
    // var extractSupportTruck = function(response) {
    //     return responseExtract('supportTruck', response);
    // }
    // var extractState = function(response) {
    //     return responseExtract('state', response);
    // }

    // var drawSupportTruck = function(supportTruck) {
    //     drawer.drawSupportTruckInMap(supportTruck, map);
    // }
    // var resolveStateSupportTruck = function(supportTruck) {
    //     // pedimos el estado con el state_id, y retornamos el movil completo
    //     return requestState(supportTruck.state_id)
    //            .then(function(response){
    //                 supportTruck.state = response.state;
    //                 delete supportTruck.state_id;                    
    //                 return supportTruck;        
    //             });
    // }

    var requestSupportTrucks = function(){
      var request = $.ajax(url + urlTrucks);
      var responseExtract = request.supportTrucks;
      var moviles = [];

      console.log(responseExtract);

      $.each(responseExtract, function(index, value){
        console.log(value.id);
        //moviles.push(value.id);
      });
      //console.log(moviles);
    }

    // comenzamos la ejecución:
    // 1. pedimos los IDs de TODOS los moviles al servidor.
    // 2. recorremos cada movil, dibujando en el mapa.
     // let moviles = [501, 402, 303, 204, 105, 606];
     // for (let i=0; i < moviles.length; i++){
     //         requestSupportTruck(moviles[i])            // pedimos un movil al servidor
     //          .then(extractSupportTruck)        // extraemos el movil de la respuesta del servidor 
     //          .then(resolveStateSupportTruck) // resolvemos el estado  
     //          .then(drawSupportTruck)       // dibujamos el movil con su estado 
     //          .done(function() {
     //              console.log("Fin.");
     //          });
     // }
     requestSupportTrucks();

    // FIN OPCIÓN 3 ***********************************************************
};

$(bootstrap);
